python-pdbfixer (1.9-7) unstable; urgency=medium

  * Team upload.
  * Use upstream commit to remove dep on python3-pkg-resources.

 -- Alexandre Detiste <tchet@debian.org>  Wed, 05 Feb 2025 07:51:17 +0100

python-pdbfixer (1.9-6) unstable; urgency=medium

  * Team upload.
  * Lower dep to a mere python3-pkg-resources,
    openmm was fixed a long time ago

 -- Alexandre Detiste <tchet@debian.org>  Mon, 27 Jan 2025 13:25:27 +0100

python-pdbfixer (1.9-5) unstable; urgency=medium

  * Team upload.
  * Add a temporary python3-setuptools dependency
    to untangle numpy 1:1.26.4+ds-13 transition.

 -- Alexandre Detiste <tchet@debian.org>  Sun, 19 Jan 2025 16:30:04 +0100

python-pdbfixer (1.9-4) unstable; urgency=medium

  * Team Upload
  * Use dh-sequence-python3
  * Add dep on python3-legacy-cgi (Closes: #1084617)

 -- Alexandre Detiste <tchet@debian.org>  Sat, 16 Nov 2024 02:02:26 +0100

python-pdbfixer (1.9-3) unstable; urgency=medium

  * Replace dependency on python3-simtk with python3-openmm (Closes: #1038954)

 -- Andrius Merkys <merkys@debian.org>  Tue, 04 Jul 2023 01:44:31 -0400

python-pdbfixer (1.9-2) unstable; urgency=medium

  * Upload to unstable.
  * Set upstream metadata fields: Repository-Browse.
  * Update standards version to 4.6.2, no changes needed.
  * python3-openmm is needed to build the package.

 -- Andrius Merkys <merkys@debian.org>  Thu, 22 Jun 2023 01:58:05 -0400

python-pdbfixer (1.9-1) experimental; urgency=medium

  * New upstream version 1.9
  * Bump copyright years.
  * Replace python3-simtk with python3-openmm.

 -- Andrius Merkys <merkys@debian.org>  Wed, 17 May 2023 03:41:33 -0400

python-pdbfixer (1.8.1-2) unstable; urgency=medium

  * Drop dependency on python3-all to build only on default Python version.
  * Limit autopkgtest to architectures where python3-simtk is installable.
  * Remove hidden tests from the binary package.
  * Wrap and sort.

 -- Andrius Merkys <merkys@debian.org>  Thu, 07 Apr 2022 02:07:24 -0400

python-pdbfixer (1.8.1-1) unstable; urgency=medium

  * Initial release (Closes: #1008622)

 -- Andrius Merkys <merkys@debian.org>  Tue, 29 Mar 2022 13:38:20 -0400
